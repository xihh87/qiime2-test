analysis/008_rarefaction/%.qzv:	analysis/007_phylogeny/%-rooted.qza	analysis/003_dada2/%-table.qza	data/%.tsv
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime diversity alpha-rarefaction \
		--i-table analysis/003_dada2/${stem}-table.qza \
		--i-phylogeny analysis/007_phylogeny/${stem}-rooted.qza \
		--p-max-depth 4000 \
		--m-metadata-file data/${stem}.tsv \
		--o-visualization ${target}

'analysis/009_beta_diversity/(.*)-unifrac-by-(.*)\.qzv':QR:	'analysis/008_phylogenetic_metrics/\1/unweighted_unifrac_distance_matrix.qza'
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime diversity beta-group-significance \
		--i-distance-matrix ${prereq} \
		--m-metadata-file data/${stem1}.tsv \
		--m-metadata-column ${stem2} \
		--o-visualization ${target} \
		--p-pairwise

analysis/009_alpha_diversity/%-evenness.qzv:Q:	analysis/008_phylogenetic_metrics/%/evenness_vector.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime diversity alpha-group-significance \
		--i-alpha-diversity ${prereq} \
		--m-metadata-file data/${stem}.tsv \
		--o-visualization ${target}

analysis/009_alpha_diversity/%-faith_pd.qzv:Q:	analysis/008_phylogenetic_metrics/%/faith_pd_vector.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime diversity alpha-group-significance \
		--i-alpha-diversity ${prereq} \
		--m-metadata-file data/${stem}.tsv \
		--o-visualization ${target}

analysis/008_phylogenetic_metrics/%/faith_pd_vector.qza \
analysis/008_phylogenetic_metrics/%/unweighted_unifrac_distance_matrix.qza \
analysis/008_phylogenetic_metrics/%/bray_curtis_pcoa_results.qza \
analysis/008_phylogenetic_metrics/%/shannon_vector.qza \
analysis/008_phylogenetic_metrics/%/rarefied_table.qza \
analysis/008_phylogenetic_metrics/%/weighted_unifrac_distance_matrix.qza \
analysis/008_phylogenetic_metrics/%/jaccard_pcoa_results.qza \
analysis/008_phylogenetic_metrics/%/weighted_unifrac_pcoa_results.qza \
analysis/008_phylogenetic_metrics/%/observed_features_vector.qza \
analysis/008_phylogenetic_metrics/%/jaccard_distance_matrix.qza \
analysis/008_phylogenetic_metrics/%/evenness_vector.qza \
analysis/008_phylogenetic_metrics/%/bray_curtis_distance_matrix.qza \
analysis/008_phylogenetic_metrics/%/unweighted_unifrac_pcoa_results.qza \
analysis/008_phylogenetic_metrics/%/unweighted_unifrac_emperor.qzv \
analysis/008_phylogenetic_metrics/%/jaccard_emperor.qzv \
analysis/008_phylogenetic_metrics/%/bray_curtis_emperor.qzv \
analysis/008_phylogenetic_metrics/%/weighted_unifrac_emperor.qzv \
:N:	analysis/008_phylogenetic_metrics/%/

analysis/008_phylogenetic_metrics/%/ \
:Q:	analysis/007_phylogeny/%-rooted.qza \
	analysis/003_dada2/%-table.qza \
	data/%.tsv
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime diversity core-metrics-phylogenetic \
		--i-phylogeny analysis/007_phylogeny/${stem}-rooted.qza \
		--i-table analysis/003_dada2/${stem}-table.qza \
		--p-sampling-depth 1103 \
		--m-metadata-file data/${stem}.tsv \
		--output-dir ${OUTDIR}/${stem}

analysis/007_phylogeny/%-aligned.qza \
analysis/007_phylogeny/%-masked.qza \
analysis/007_phylogeny/%-unrooted.qza \
:N:	analysis/007_phylogeny/%-rooted.qza

analysis/007_phylogeny/%-rooted.qza \
:Q:	analysis/003_dada2/%-representative.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime phylogeny align-to-tree-mafft-fasttree \
		--i-sequences ${prereq} \
		--o-alignment ${OUTDIR}/${stem}-aligned.qza \
		--o-masked-alignment ${OUTDIR}/${stem}-masked.qza \
		--o-tree ${OUTDIR}/${stem}-unrooted.qza \
		--o-rooted-tree ${OUTDIR}/${stem}-rooted.qza

analysis/006_representative_sequences/%-deblur.qzv:Q:	analysis/004_deblur_denoise/%-representative.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime feature-table tabulate-seqs \
		--i-data ${prereq} \
		--o-visualization ${target}

analysis/006_representative_sequences/%-dada2.qzv:Q:	analysis/003_dada2/%-representative.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime feature-table tabulate-seqs \
		--i-data ${prereq} \
		--o-visualization ${target}

analysis/006_features/%-dada2.qzv:Q:	analysis/003_dada2/%-table.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime feature-table summarize \
		--i-table ${prereq} \
		--o-visualization ${target} \
		--m-sample-metadata-file data/${stem}.tsv

analysis/006_features/%-deblur.qzv:Q:	analysis/004_deblur_denoise/%-table.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime feature-table summarize \
		--i-table ${prereq} \
		--o-visualization ${target} \
		--m-sample-metadata-file data/${stem}.tsv

analysis/005_deblur_stats/%.qzv:Q:	analysis/004_deblur_denoise/%-stats.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime deblur visualize-stats \
		--i-deblur-stats ${prereq} \
		--o-visualization ${target}

analysis/004_deblur_tabulate/%.qzv:Q:	analysis/003_deblur_filter/%-stats.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime metadata tabulate \
		--m-input-file ${prereq} \
		--o-visualization ${target}

analysis/004_deblur_denoise/%-stats.qza \
analysis/004_deblur_denoise/%-table.qza \
:N:	analysis/003_deblur_filter/%.qza

analysis/004_deblur_denoise/%-representative.qza \
:Q:	analysis/003_deblur_filter/%.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime deblur denoise-16S \
		--i-demultiplexed-seqs ${prereq} \
		--p-trim-length 120 \
		--o-representative-sequences ${OUTDIR}/${stem}-representative.qza \
		--o-table ${OUTDIR}/${stem}-table.qza \
		--p-sample-stats \
		--o-stats ${OUTDIR}/${stem}-stats.qza

analysis/003_deblur_filter/%-stats.qza\
:N:	analysis/003_deblur_filter/%.qza

analysis/003_deblur_filter/%.qza:Q:	analysis/002_demux/%.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime quality-filter q-score \
		--i-demux ${prereq} \
		--o-filtered-sequences ${OUTDIR}/${stem}.qza \
		--o-filter-stats ${OUTDIR}/${stem}-stats.qza

analysis/004_tabulate/%.qzv:Q:	analysis/003_dada2/%-stats.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime metadata tabulate \
		--m-input-file ${prereq} \
		--o-visualization ${target}

analysis/003_summarize/%.qzv:Q:	analysis/002_demux/%.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime demux summarize \
		--i-data ${prereq} \
		--o-visualization ${target}

analysis/003_dada2/%-table.qza \
analysis/003_dada2/%-stats.qza \
:N:	analysis/003_dada2/%-representative.qza

analysis/003_dada2/%-representative.qza \
:Q:	analysis/002_demux/%.qza
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime dada2 denoise-single \
		--i-demultiplexed-seqs ${prereq} \
		--p-trim-left 0 \
		--p-trunc-len 120 \
		--o-representative-sequences ${OUTDIR}/${stem}-representative.qza \
		--o-table ${OUTDIR}/${stem}-table.qza \
		--o-denoising-stats ${OUTDIR}/${stem}-stats.qza

analysis/002_demux/%.qza:Q:	analysis/001_import/%.qza	data/%.tsv
	set -x
	OUTDIR="$(dirname ${target})"
	mkdir -p ${OUTDIR}
	qiime demux emp-single \
		--i-seqs analysis/001_import/${stem}.qza \
		--m-barcodes-file data/${stem}.tsv \
		--m-barcodes-column barcode-sequence \
		--o-per-sample-sequences ${target} \
		--o-error-correction-details ${OUTDIR}/${stem}-details.qza

analysis/001_import/%.qza:Q:	data/%/
	set -x
	mkdir -p "$(dirname ${target})"
	qiime tools import \
		--type EMPSingleEndSequences \
		--input-path ${prereq} \
		--output-path ${target}

clean:V:
	rm -rf analysis

params.metadata = "data/moving-pictures.tsv"
params.data = "data/moving-pictures"

data = Channel.fromPath(params.data, type: 'dir')
	.map { dir -> tuple(dir.baseName, dir) }

process qiime_import {
	publishDir "analysis/001_import/", mode: 'link'

	input:
	tuple id, dir from data

	output:
	tuple id, file("${id}.qza") into imports

	"""
	qiime tools import \
		--type EMPSingleEndSequences \
		--input-path "${dir}" \
		--output-path "${dir.baseName}.qza"
	"""
}

process demux {
	publishDir "analysis/002_demux/", mode: 'link'

	input:
	file m from Channel.fromPath(params.metadata, type: 'file')
	tuple id, file from imports

	output:
	tuple id, file("${id}-demux.qza"), file("${id}-details.qza") into demux

	"""
	qiime demux emp-single \
		--i-seqs "${file}" \
		--m-barcodes-file "${m}" \
		--m-barcodes-column barcode-sequence \
		--o-per-sample-sequences "${id}-demux.qza" \
		--o-error-correction-details "${id}-details.qza"
	"""
}

process dada {
	publishDir "analysis/003_dada2", mode: 'link'

	input:
	tuple id, demux, stats from demux

	output:
	tuple id, file("${id}-representative.qza") into dada_seqs
	tuple id, file("${id}-representative.qza") into dada_seqs2
	tuple id, file("${id}-representative.qza"),
		file("${id}-table.qza"),
		file("${id}-stats.qza") into dada_table
	tuple id, file("${id}-table.qza") into tablem

	"""
	qiime dada2 denoise-single \
		--i-demultiplexed-seqs "${demux}" \
		--p-trim-left 0 \
		--p-trunc-len 120 \
		--o-representative-sequences "${id}-representative.qza" \
		--o-table "${id}-table.qza" \
		--o-denoising-stats "${id}-stats.qza"
	"""
}

process dada_features {
	publishDir "analysis/006_features", mode: 'link'

	input:
	tuple id, sequences, table, stats from dada_table
	file mdata from Channel.fromPath(params.metadata, type: "file")

	output:
	tuple id, file("${id}-dada2.qzv") into dada_features

	"""
	qiime feature-table summarize \
		--i-table "${table}" \
		--o-visualization "${id}-dada2.qzv" \
		--m-sample-metadata-file "${mdata}"
	"""
}

process dada_sequences {
	publishDir "analysis/006_representative_sequences", mode: 'link'

	input:
	tuple id, sequences from dada_seqs

	output:
	tuple id, file("${id}-dada2.qzv")

	"""
	qiime feature-table tabulate-seqs \
		--i-data ${sequences} \
		--o-visualization "${id}-dada2.qzv"
	"""
}

process phylogeny {
	publishDir "analysis/007_philogeny", mode: 'link'

	input:
	tuple id, sequences from dada_seqs2

	output:
	tuple id, file("${id}-aligned.qza"), file("${id}-masked.qza"),
		file("${id}-unrooted.qza"), file("${id}-rooted.qza") into phylogeny
	tuple id, file("${id}-rooted.qza") into phym

	"""
	qiime phylogeny align-to-tree-mafft-fasttree \
		--i-sequences ${sequences} \
		--o-alignment ${id}-aligned.qza \
		--o-masked-alignment ${id}-masked.qza \
		--o-tree ${id}-unrooted.qza \
		--o-rooted-tree ${id}-rooted.qza
	"""
}

process phylogeny_metrics {
	publishDir "analysis/008_phylogenetic_metrics/", mode: 'copy'

	input:
	tuple id, rooted_tree from phym
	file mdata from Channel.fromPath(params.metadata, type: 'file')
	tuple id, table from tablem

	output:
	tuple id, path("${id}") into metrics

	"""
	qiime diversity core-metrics-phylogenetic \
		--i-phylogeny ${rooted_tree} \
		--i-table ${table} \
		--p-sampling-depth 1103 \
		--m-metadata-file ${mdata} \
		--output-dir ${id}
	"""
}

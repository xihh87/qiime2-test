Flujo de trabajo por defecto de QIIME2 en varios gestores de trabajo.

`qiime2-test` ejemplifica cómo se implementan los conceptos del curso «Bioinformática Aplicada a Tu Proyecto»
para los estudiantes de que quieren aprender a hacer flujos de trabajo autocontenidos
y a ofrece la experiencia de reproducir un análisis de inicio a fin.

# Uso

Descargar la información:

```
bin/dl-data
```

## Ejecutar mk

Si `qiime` está instalado, únicamente ejecutar:

```
$ bin/targets | xargs mk
```

Para usar `qiime` desde docker, agregar al PATH:

```
$ cp cmd/qiime /directorio/en/tu/PATH
$ chmod +x /directorio/en/tu/PATH/qiime
```

Nextflow falla si `qiime` es ejecutable en este directorio.

## Ejecutar Nextflow

Nextflow ya está configurado para usar docker pero se puede desactivar cambiando `enabled = true`
por `enabled = false` en `nextflow.config`.

```
nextflow run qiime.nf
```

# Dependencias

-   `curl`
-   `qiime` o `docker`
-   `mk` (usualmente en paquetes plan9port o 9base)
-   `nextflow`

# Documentación suplementaria

-   [Tutorial introductorio para QIIME2](https://docs.qiime2.org/2020.6/tutorials/moving-pictures/ ).
